INSERT INTO role(name) VALUES ('ROLE_ADMIN');
INSERT INTO role(name) VALUES ('ROLE_DEVELOPER');
INSERT INTO role(name) VALUES ('ROLE_HR');
INSERT INTO role(name) VALUES ('ROLE_FINANCE');
INSERT INTO company(name, domain) VALUES('Microsoft','IT');
INSERT INTO company(name, domain) VALUES('Amazon', 'e-Commerce');
INSERT INTO company(name, domain) VALUES('McDonald s', 'Food');
-- Microsoft users
INSERT INTO user(name, username, password, email, company_id) VALUES('John Microsoft', 'john.microsoft', '$2y$12$vceIICYrxvzktCQ9DeT3FuSOBirIw128tMz4B9.y35twbF0/swkvS', 'john@microsoft.com', 1);
INSERT INTO user(name, username, password, email, company_id) VALUES('Mike Microsoft', 'mike.microsoft', 'password3', 'mike@microsoft.com', 1);
INSERT INTO user(name, username, password, email, company_id) VALUES('Anna Microsoft', 'anna.microsoft', 'password3', 'anna@microsoft.com', 1);
INSERT INTO user(name, username, password, email, company_id) VALUES('Emma Microsoft', 'emma.microsoft', 'password4', 'emma@microsoft.com', 1);
INSERT INTO user_role(user_id, role_id) VALUES (1,1);
INSERT INTO user_role(user_id, role_id) VALUES (1,2);
INSERT INTO user_role(user_id, role_id) VALUES (1,3);
INSERT INTO user_role(user_id, role_id) VALUES (2,2);
INSERT INTO user_role(user_id, role_id) VALUES (2,3);
INSERT INTO user_role(user_id, role_id) VALUES (3,3);
INSERT INTO user_role(user_id, role_id) VALUES (4,4);
-- Amazon users
INSERT INTO user(name, username, password, email, company_id) VALUES('William Amazon', 'william.amazon', 'password1', 'william@amazon.com', 2);
INSERT INTO user(name, username, password, email, company_id) VALUES('James Amazon', 'james.amazon', 'password1', 'james@amazon.com', 2);
INSERT INTO user(name, username, password, email, company_id) VALUES('Jack Amazon', 'jack.amazon', 'password1', 'jack@amazon.com', 2);
INSERT INTO user(name, username, password, email, company_id) VALUES('Madison Amazon', 'madison.amazon', 'password1', 'madison@amazon.com', 2);
INSERT INTO user(name, username, password, email, company_id) VALUES('Ellie Amazon', 'ellie.amazon', 'password1', 'ellie@amazon.com', 2);
INSERT INTO user(name, username, password, email, company_id) VALUES('Anna Amazon', 'anabelle.amazon', 'password1', 'anabelle@amazon.com', 2);
INSERT INTO user_role(user_id, role_id) VALUES (5,1);
INSERT INTO user_role(user_id, role_id) VALUES (5,2);
INSERT INTO user_role(user_id, role_id) VALUES (5,3);
INSERT INTO user_role(user_id, role_id) VALUES (6,2);
INSERT INTO user_role(user_id, role_id) VALUES (7,3);
INSERT INTO user_role(user_id, role_id) VALUES (8,4);
INSERT INTO user_role(user_id, role_id) VALUES (9,3);
INSERT INTO user_role(user_id, role_id) VALUES (10,3);
-- McDonald s users
INSERT INTO user(name, username, password, email, company_id) VALUES('Mary McDonald s', 'mary.mc', 'password1', 'mary@mcdonalds.com', 3);
INSERT INTO user(name, username, password, email, company_id) VALUES('Christian McDonald s', 'christian.mc', 'password1', 'christian@mcdonalds.com', 3);
INSERT INTO user(name, username, password, email, company_id) VALUES('Robert McDonald s', 'robert.mc', 'password1', 'robert@mcdonalds.com', 3);
INSERT INTO user_role(user_id, role_id) VALUES (11,1);
INSERT INTO user_role(user_id, role_id) VALUES (11,2);
INSERT INTO user_role(user_id, role_id) VALUES (11,4);
INSERT INTO user_role(user_id, role_id) VALUES (12,2);
INSERT INTO user_role(user_id, role_id) VALUES (13,3);

