package com.sda.spring.web;

import com.sda.spring.web.models.User;
import com.sda.spring.web.repositories.CompanyRepository;
import com.sda.spring.web.repositories.RoleRepositories;
import com.sda.spring.web.repositories.UserRepository;
import com.sda.spring.web.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@SpringBootApplication
@RestController
public class WebApplication implements CommandLineRunner {

	@Autowired
	private RoleRepositories roleRepositories;

	@Autowired
	private UserRepository userRepository;

//	@Autowired
//	private UserService userService;

	@Autowired
	CompanyRepository companyRepository;



	public static void main(String[] args) {
		SpringApplication.run(WebApplication.class, args);

	}

	@GetMapping("/")
	public String test() {
		return String.valueOf(roleRepositories.count());
	}

	@Override
	@Transactional
	public void run(String... args) throws Exception {
//		System.out.println(userRepository.findByNameEquals("John").get(0));


//		User user = new User("Ana", "are", "mere", "mck");
//		user.setCompany(companyRepository.findById(1L).get());
////		userService.save(user);
//		System.out.println(userRepository.findById(2L));


	}
}


