package com.sda.spring.web.mappers;

import com.sda.spring.web.dto.RoleDto;
import com.sda.spring.web.models.Role;

public class RoleMapper {
    public static Role toEntity(RoleDto roleDto) {
        Role role = new Role();
        role.setId(roleDto.getId());
        role.setName(roleDto.getName());
        return role;
    }
    public static RoleDto toDto(Role role) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setName(role.getName());
        return roleDto;
    }
}
