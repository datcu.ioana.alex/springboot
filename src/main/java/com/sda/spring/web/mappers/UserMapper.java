package com.sda.spring.web.mappers;

import com.sda.spring.web.dto.UserDto;
import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.models.Company;
import com.sda.spring.web.models.User;
import com.sda.spring.web.repositories.CompanyRepository;

import java.util.stream.Collectors;

public class UserMapper {
    public static User toEntity(UserDto userDto, CompanyRepository companyRepository) {
        User user = new User();
        Company company = companyRepository
                .findById(userDto.getCompanyId())
                .orElseThrow(() -> new ResourceNotFoundException("Company does not exists"));

        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setCompany(company);
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        return user;
    }

    public static UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setCompanyId(user.getCompany().getId());
        userDto.setRoleDtoList(user.getRoles()
                .stream()
                .map(RoleMapper::toDto)
                .collect(Collectors.toList()));
        return userDto;
    }
}
