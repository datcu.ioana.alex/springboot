package com.sda.spring.web.mappers;

import com.sda.spring.web.dto.CompanyDto;
import com.sda.spring.web.models.Company;

import java.util.stream.Collectors;

public class CompanyMapper {

    public static Company toEntity(CompanyDto companyDto) {
        Company company = new Company();

        company.setId(companyDto.getId());
        company.setName(companyDto.getName());
        company.setDomain(companyDto.getDomain());

        return company;
    }

    public static CompanyDto toDto(Company company) {
        CompanyDto companyDto = new CompanyDto();

        companyDto.setId(company.getId());
        companyDto.setName(company.getName());
        companyDto.setDomain(company.getDomain());
        companyDto.setUserDtoList(company.getUserList()
                .stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList()));

        return companyDto;
    }
}
