package com.sda.spring.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends BaseException {

    private static final long serialVersionUID = 5759256102627568805L;

    public BadRequestException(String message) {
        super(message);
    }
}
