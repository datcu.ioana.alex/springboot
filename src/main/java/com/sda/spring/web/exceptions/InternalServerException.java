package com.sda.spring.web.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends BaseException {

    private static final long serialVersionUID = -1734878642189260451L;

    public InternalServerException(String message) {
        super(message);
    }
}
