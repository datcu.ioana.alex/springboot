package com.sda.spring.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ChangePasswordException extends BaseException {
    public ChangePasswordException(String message) {
        super(message);
    }
}
