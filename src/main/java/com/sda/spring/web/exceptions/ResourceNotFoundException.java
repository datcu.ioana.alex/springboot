package com.sda.spring.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends BaseException {

    private static final long serialVersionUID = 6594495166212789619L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
