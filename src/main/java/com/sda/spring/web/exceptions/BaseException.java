package com.sda.spring.web.exceptions;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BaseException extends RuntimeException{

    private static final long serialVersionUID = -754355743159670342L;

    public BaseException(String message) {
        super(message);
    }

    public HttpStatus getHttpStatus() {
        ResponseStatus annotation = this.getClass().getAnnotation(ResponseStatus.class);
        if(annotation != null) {
            return annotation.value();
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
