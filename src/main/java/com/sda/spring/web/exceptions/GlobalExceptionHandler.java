package com.sda.spring.web.exceptions;

import com.sda.spring.web.dto.ErrorResponse;
import com.sda.spring.web.exceptions.BaseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(BaseException.class)
    public final ResponseEntity<ErrorResponse> handledCustomException(BaseException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), (exception.getHttpStatus()));
    }


    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> handledException(Exception exception) {
        return new ResponseEntity<>(new ErrorResponse("Exception was thrown" + exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
