package com.sda.spring.web.repositories;

import com.sda.spring.web.models.Role;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepositories extends JpaRepository<Role, Long> {

    Optional<Role> findByName(String roleName);
    List<Role> findAllByNameContaining(String contains);
    void deleteByName(String roleName);

    @Modifying(flushAutomatically=true,clearAutomatically=true)
    @Query(value = "UPDATE Role r SET r.name = :name WHERE r.id = :roleId")
    void updateByName(@Param("roleId")Long roleId, @Param("name")String name);

    List<Role> findAllByOrderByName(Sort name);
    boolean existsByName(String name);
}

