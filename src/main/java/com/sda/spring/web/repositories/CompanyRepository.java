package com.sda.spring.web.repositories;

import com.sda.spring.web.models.Company;
import com.sda.spring.web.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {
    Optional<Company> findById(Long id);
    List<Company> findByDomainContainingIgnoreCase(String text);
}
