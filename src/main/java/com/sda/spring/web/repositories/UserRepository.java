package com.sda.spring.web.repositories;

import com.sda.spring.web.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findById(Long id);

    List<User> findByNameEquals(String name);

    Optional<User> findByEmail(String email);

    List<User> findByName(String name);

    //Select * from USERS u WHERE to LOWER u.name like to LOWER'%name%'
    List<User> findByNameContainingIgnoreCase(String name);

    //Select * from USERS u WHERE to LOWER u.name like to LOWER 'prefix%'
    List<User> findByNameStartingWithIgnoreCase(String prefix);

    List<User> findAllByCompanyId(Long Id);

    List<User> findAllByCompanyIdOrderByUsername(Long Id);

    List<User> findAllByCompanyIdAndNameContainingIgnoreCase(Long Id, String containingString);

    List<User> findByEmailEndsWithOrderByEmail(String emailEnding);

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameOrEmail(String username, String email);

    boolean existsByNameContaining(String name);

    long countByEmailEndsWith(String emailSuffix);

}
