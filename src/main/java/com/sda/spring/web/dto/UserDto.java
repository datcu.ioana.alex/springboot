package com.sda.spring.web.dto;

import com.sda.spring.web.models.Company;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UserDto {
    private Long id;


    private String name;

    private String username;

    @Email
    private String email;
    private List<RoleDto> roleDtoList;

    private Long companyId;
}
