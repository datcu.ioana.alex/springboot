package com.sda.spring.web.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class ResetPasswordRequestDto {
    @Email
    @NotBlank
    private String email;
}
