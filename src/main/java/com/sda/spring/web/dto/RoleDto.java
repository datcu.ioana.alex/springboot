package com.sda.spring.web.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RoleDto {
    private Long id;

    private String name;


}
