package com.sda.spring.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class ChangePasswordRequestDto {
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    @Size(min = 6, max = 40)
    @NotBlank
    private String confirmPassword;

}
