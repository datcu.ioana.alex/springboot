package com.sda.spring.web.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class CompanyDto {
    private Long id;
    private String name;
    private String domain;
    private List<UserDto> userDtoList;

}
