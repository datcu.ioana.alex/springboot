package com.sda.spring.web.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class LoginRequestDto {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
