package com.sda.spring.web.dto;

import lombok.Data;

@Data
public class ResetPasswordDto {
    private String token;
    private String newPassword;
    private String newPasswordConfirm;
}
