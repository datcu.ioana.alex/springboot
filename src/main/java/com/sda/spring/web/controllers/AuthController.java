package com.sda.spring.web.controllers;

import com.sda.spring.web.dto.ChangePasswordRequestDto;
import com.sda.spring.web.dto.LoginRequestDto;
import com.sda.spring.web.dto.ResetPasswordDto;
import com.sda.spring.web.dto.ResetPasswordRequestDto;
import com.sda.spring.web.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController extends BaseController {
    private static final String API_NAME = "auth";

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(AuthController.API_NAME + "/login")
    public ResponseEntity<Void> login(@Valid @RequestBody LoginRequestDto login) {
        authService.login(login);
        return ResponseEntity.ok().build();
    }

    @PostMapping(AuthController.API_NAME + "/change-password")
    public ResponseEntity<String> changePassword(@Valid @RequestBody ChangePasswordRequestDto changePasswordRequestDto) {
        authService.changePassword(changePasswordRequestDto);
        return ResponseEntity.ok("Password changed successfully");
    }

    @PostMapping(AuthController.API_NAME + "/reset-password-link")
    public ResponseEntity<String> resetPasswordLink(@Valid @RequestBody ResetPasswordRequestDto dto) {
        return ResponseEntity.ok(authService.forgotPasswordResetLink(dto.getEmail()));
    }

    @PostMapping(AuthController.API_NAME + "/reset-password")
    public ResponseEntity<Void> resetPassword(ResetPasswordDto dto) {
        authService.forgotPassword(dto.getToken(), dto.getNewPassword(), dto.getNewPasswordConfirm());
        return ResponseEntity.ok().build();
    }
}
