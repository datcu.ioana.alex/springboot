package com.sda.spring.web.controllers;

import com.sda.spring.web.dto.UserDto;
import com.sda.spring.web.exceptions.BadRequestException;
import com.sda.spring.web.services.CompanyService;
import com.sda.spring.web.services.EmailService;
import com.sda.spring.web.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController extends BaseController{
    private static final String API_NAME = "users";

    UserService userService;
    EmailService emailService;

    @Autowired
    public UserController(UserService userService, EmailService emailService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    @PostMapping(UserController.API_NAME)
    public ResponseEntity<String> createUser(@Valid @RequestBody UserDto userDto) {
        userService.createUser(userDto);
        return ResponseEntity.ok("User created");
    }

    @GetMapping(UserController.API_NAME + "/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @GetMapping(UserController.API_NAME)
    public ResponseEntity<List<UserDto>> findUsers(@RequestParam(value = "name", required = false) String name) {
        emailService.sendEmail("datcu.ioana.alex@gmail.com", "test subject", "test message");
        return ResponseEntity.ok(userService.findUsers(name));
    }

    @GetMapping(UserController.API_NAME + "/company-id/{id}")
    public ResponseEntity<List<UserDto>> findUsersByCompany(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(userService.findAllUsersFromCompany(id));
    }
//
//    @GetMapping(UserController.API_NAME)
//    public ResponseEntity<UserDto> findUserByEmailOrUsername(@RequestParam(value = "email", required = false) String email,
//                                                             @RequestParam(value = "username", required = false) String username) {
//        return ResponseEntity.ok(userService.getUserByEmailOrUsername(email, username));
//    }

    @PutMapping(UserController.API_NAME + "/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Long id,
                                              @RequestBody @Valid UserDto userDto) {
        return new ResponseEntity<>
                (userService.updateUser(id, userDto),
                        HttpStatus.ACCEPTED);
    }

    @DeleteMapping(UserController.API_NAME + "/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.accepted().build();
    }
}
