package com.sda.spring.web.controllers;

import com.sda.spring.web.dto.RoleDto;
import com.sda.spring.web.models.User;
import com.sda.spring.web.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoleController extends BaseController{
    public static final String API_NAME = "roles";

    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(RoleController.API_NAME)
    public ResponseEntity<List<RoleDto>> findRoles() {
        return ResponseEntity.ok(roleService.findRoles());
    }

    @GetMapping(RoleController.API_NAME + "/{id}")
    public ResponseEntity<RoleDto> findRoleById(@PathVariable Long id) {
        return ResponseEntity.ok(roleService.findRoleById(id));
    }

    @PutMapping(RoleController.API_NAME + "/{id}")
    public ResponseEntity<RoleDto> updateRole(@PathVariable Long id,
                                              @RequestBody RoleDto roleDto) {
        return new ResponseEntity<>(roleService.updateRole(id, roleDto),
                        HttpStatus.ACCEPTED);
    }

    @DeleteMapping(RoleController.API_NAME + "/{name}")
    public ResponseEntity<?> deleteRole(@PathVariable String name) {
        roleService.deleteRole(name);
        return ResponseEntity.accepted().build();
    }
}
