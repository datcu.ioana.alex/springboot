package com.sda.spring.web.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BaseController.API_V1)
public class BaseController {
    public static final String API_V1 = "/api/v1";

}
