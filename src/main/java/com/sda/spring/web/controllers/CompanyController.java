package com.sda.spring.web.controllers;

import com.sda.spring.web.dto.CompanyDto;
import com.sda.spring.web.mappers.CompanyMapper;
import com.sda.spring.web.models.Company;
import com.sda.spring.web.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CompanyController extends BaseController {
    public static final String API_NAME = "companies";

    private CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping(CompanyController.API_NAME + "/{id}")
    @Secured("ADMIN")
    public ResponseEntity<CompanyDto> getCompany(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.getCompanyById(id));
    }

    @GetMapping(CompanyController.API_NAME)
    public ResponseEntity<List<CompanyDto>> getCompanies(
            @RequestParam(value = "domain", required = false) String domain) {
        return ResponseEntity.ok(companyService.getCompanies(domain));
    }

    @PostMapping(CompanyController.API_NAME)
    public ResponseEntity<String> createCompany(@Valid @RequestBody CompanyDto companyDto) {
        companyService.createCompany(companyDto);
        return ResponseEntity.ok("Company created");
    }

    @PutMapping(CompanyController.API_NAME + "/{id}")

    public ResponseEntity<CompanyDto> updateCompany(@PathVariable Long id,
                                                    @RequestBody @Valid CompanyDto companyDto) {
        return new ResponseEntity<>(companyService.updateCompany(id, companyDto),
                HttpStatus.ACCEPTED);
    }

    @DeleteMapping(CompanyController.API_NAME + "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        companyService.deleteCompany(id);
        return ResponseEntity.accepted().build();
    }
}
