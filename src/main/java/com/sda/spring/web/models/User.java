package com.sda.spring.web.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "username")
    private String username;

    @Column(name = "password")

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    @Column(name = "email", unique = true)

    private String email;

    @ManyToMany
    @JoinTable(name = "user_role",
    joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "role_id"))
    @ToString.Exclude
    private List<Role> roles;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;


    public User(String name, String username, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.username = username;
        roles = new ArrayList<>();
    }
}
