package com.sda.spring.web.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@ToString
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    private String domain;

    @OneToMany(mappedBy = "company", cascade = CascadeType.REMOVE)
    @ToString.Exclude
    private List<User> userList = new ArrayList<>();

    public Company(String name, String domain) {
        this.name = name;
        this.domain = domain;
        this.userList = new ArrayList<>();
    }
}
