package com.sda.spring.web.services;

import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.models.PasswordResetToken;
import com.sda.spring.web.models.User;
import com.sda.spring.web.repositories.PasswordResetTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
public class PasswordResetTokenService {
    private final PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    public PasswordResetTokenService(PasswordResetTokenRepository passwordResetTokenRepository) {
        this.passwordResetTokenRepository = passwordResetTokenRepository;
    }

    public boolean isValidToken(String token) {
        Optional<PasswordResetToken> resetToken = passwordResetTokenRepository.findByToken(token);
        return resetToken
                .map(passwordResetToken -> passwordResetToken.getExpireDate().isAfter(Instant.now()))
                .orElse(false);
    }

    public PasswordResetToken createOrUpdateToken(User user) {
        String token = UUID.randomUUID().toString();
        PasswordResetToken passwordResetToken =  passwordResetTokenRepository
                .findByUser(user)
                .orElse(new PasswordResetToken(user));
        passwordResetToken.setToken(token);
        passwordResetToken.setExpireDate(Instant.now().plusSeconds(3600));
        return passwordResetTokenRepository.save(passwordResetToken);
    }

    public PasswordResetToken findByToken(String token) {
        return passwordResetTokenRepository
                .findByToken(token)
                .orElseThrow(() -> new ResourceNotFoundException("Not found token"));
    }
}
