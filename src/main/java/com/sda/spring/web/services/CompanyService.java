package com.sda.spring.web.services;

import com.sda.spring.web.dto.CompanyDto;
import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.mappers.CompanyMapper;
import com.sda.spring.web.models.Company;
import com.sda.spring.web.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public CompanyDto getCompanyById(Long id) {
        return companyRepository.findById(id)
                .map(CompanyMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Company with id " + id + " does not exist"));
    }

    public List<CompanyDto> getCompanies(String domain) {
        if (domain == null) {
            return getAllCompanies();
        } else {
            return getCompanyByDomainContainingString(domain);
        }
    }

    @Transactional
    public void createCompany(CompanyDto companyDto) {
        Company company = CompanyMapper.toEntity(companyDto);
        companyRepository.save(company);
    }

    public CompanyDto updateCompany(Long id, CompanyDto companyDto) {
        Company company = companyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Company with id " + id + " does not exist"));
        company.setName(companyDto.getName());
        company.setDomain(companyDto.getDomain());
        companyRepository.save(company);
        return CompanyMapper.toDto(company);
    }

    public void deleteCompany(Long id) {
        companyRepository.deleteById(id);
    }

    private List<CompanyDto> getCompanyByDomainContainingString(String text) {
        return companyRepository.findByDomainContainingIgnoreCase(text).stream()
                .map(CompanyMapper::toDto)
                .collect(Collectors.toList());

    }

    private List<CompanyDto> getAllCompanies() {
        return companyRepository.findAll()
                .stream()
                .map(CompanyMapper::toDto)
                .collect(Collectors.toList());
    }


}
