package com.sda.spring.web.services;

import com.sda.spring.web.dto.ChangePasswordRequestDto;
import com.sda.spring.web.dto.LoginRequestDto;
import com.sda.spring.web.exceptions.ChangePasswordException;
import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.models.User;
import com.sda.spring.web.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final PasswordResetTokenService tokenService;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, UserRepository userRepository, PasswordEncoder passwordEncoder, PasswordResetTokenService tokenService) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenService = tokenService;
    }

    public void login(LoginRequestDto login) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public void changePassword(ChangePasswordRequestDto requestDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository
                .findByUsername(authentication.getName())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        if (checkSamePassword(requestDto.getPassword(), requestDto.getConfirmPassword())
                && checkPasswordIsDifferent(requestDto.getPassword(), user.getPassword())) {
            user.setPassword(passwordEncoder.encode(requestDto.getPassword()));
            userRepository.save(user);

        } else {
            throw new ChangePasswordException("no no, mister superman not home");
        }
    }

    public String forgotPasswordResetLink(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("email does not exist"));
        return tokenService.createOrUpdateToken(user).getToken();
        //return token pe mail
    }

    public void forgotPassword(String token, String newPassword, String newPasswordConfirm) {
        if (tokenService.isValidToken(token)) {
            User user = tokenService.findByToken(token).getUser();
            //to do extract method - updatePassword
            if (checkSamePassword(newPassword, newPasswordConfirm)
                    && checkPasswordIsDifferent(newPassword, user.getPassword())) {
                user.setPassword(passwordEncoder.encode(newPassword));
                userRepository.save(user);

            } else {
                throw new ChangePasswordException("no no, mister superman not home");
            }
        }
    }

    //todo pentru n parole
    //create entity to save last pass
    //cand un user isi schimba pass create new row in table
    //cand reset create new row/update tabela
    //create service si repository
    //de fiecare data cand atingem limita de n pass o stergem pe cea mai veche si o inseram pe ultima -  FIFO


    //todo email
    //sa trimitem email

    private boolean checkSamePassword(String password, String confirmedPassword) {
        return password.equals(confirmedPassword);
    }

    private boolean checkPasswordIsDifferent(String newPassword, String dbPassword) {
        return !passwordEncoder.matches(newPassword, dbPassword);
    }

}
