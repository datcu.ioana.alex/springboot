package com.sda.spring.web.services;

import com.sda.spring.web.dto.UserDto;
import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.mappers.UserMapper;
import com.sda.spring.web.models.Company;
import com.sda.spring.web.models.User;
import com.sda.spring.web.repositories.CompanyRepository;
import com.sda.spring.web.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;

    @Autowired
    public UserService(UserRepository userRepository, CompanyRepository companyRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
    }

    public List<UserDto> findUsers(String name) {
        if (name == null) {
            return findAllUsers();
        }
        else {
            return findByNameContainingIgnoreCase(name);
        }
    }

    public UserDto getUserById(Long id) {
        return userRepository
                .findById(id)
                .map(UserMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("User with id " + id + " does not exist"));
    }

   public List<UserDto> findAllUsersFromCompany(Long companyId) {
        return userRepository.findAllByCompanyId(companyId)
                .stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
   }

   public UserDto getUserByEmailOrUsername(String email, String username) {
            return userRepository.findByUsernameOrEmail(email, username)
                    .map(UserMapper::toDto)
                    .orElseThrow(() -> new ResourceNotFoundException("Company with id " + email + username + " does not exist"));

   }

    public void createUser(UserDto userDto) {
        Long id = userDto.getId();
        Company company = companyRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Company does not exists"));
        User user = UserMapper.toEntity(userDto, companyRepository);
        userRepository.save(user);
    }

    public UserDto updateUser(Long id, UserDto userDto) {
        User user = userRepository.findById(id)
                .orElseThrow(() ->  new ResourceNotFoundException("Not found"));
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        userRepository.save(user);
        return UserMapper.toDto(user);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    private List<UserDto> findAllUsers() {
        return userRepository
                .findAll()
                .stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
    }

    private List<UserDto> findByNameContainingIgnoreCase(String name) {
        return userRepository
                .findByNameContainingIgnoreCase(name)
                .stream()
                .map(UserMapper::toDto)
                .collect(Collectors.toList());
    }

}
