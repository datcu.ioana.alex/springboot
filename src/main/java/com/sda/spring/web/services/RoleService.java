package com.sda.spring.web.services;

import com.sda.spring.web.dto.RoleDto;
import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.mappers.CompanyMapper;
import com.sda.spring.web.mappers.RoleMapper;
import com.sda.spring.web.models.Role;
import com.sda.spring.web.repositories.RoleRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService {
    private final RoleRepositories roleRepositories;

    @Autowired
    public RoleService(RoleRepositories roleRepositories) {
        this.roleRepositories = roleRepositories;
    }

    public List<RoleDto> findRoles() {
        return roleRepositories
                .findAll()
                .stream()
                .map(RoleMapper::toDto)
                .collect(Collectors.toList());
    }

    public RoleDto findRoleById(Long id) {
        return roleRepositories
                .findById(id)
                .map(RoleMapper::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Role " + id + " not found"));
    }

    public RoleDto updateRole(Long id, RoleDto roleDto) {
        Role role = roleRepositories.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found"));
        role.setName(roleDto.getName());
        roleRepositories.save(role);
        return RoleMapper.toDto(role);
    }

    @Transactional
    public void deleteRole(String name) {
        roleRepositories.deleteByName(name);
    }
}
