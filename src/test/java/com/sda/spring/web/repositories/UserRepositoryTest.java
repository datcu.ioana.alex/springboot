package com.sda.spring.web.repositories;

import com.sda.spring.web.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByName() {
        String validName = "John Microsoft";
        List<User> users = userRepository.findByNameEquals(validName);

        assertEquals(1, users.size());
        assertEquals(validName, users.get(0).getName());
    }

    @Test
    public void findByNameWithInvalidInputShouldReturnEmptyList() {
        String invalidName = "JJ";
        List<User> users = userRepository.findByNameEquals(invalidName);

        assertTrue(users.isEmpty());
    }

    @Test
    public void testFindByNameStartingWithIgnoreCase() {
        String validName = "anna";
        List<User> users = userRepository.findByNameStartingWithIgnoreCase(validName);

        assertEquals(2, users.size());
        users.forEach(user -> assertTrue(user.getName().toLowerCase().startsWith(validName)));
    }

    @Test
    public void testFindAllUsersByEmailEndsWith() {
        String emailSuffix = "@microsoft.com";
        List<User> users = userRepository.findByEmailEndsWithOrderByEmail(emailSuffix);
        List<User> sortedUsers = users.
                stream().
                sorted(Comparator.comparing(User::getEmail)).
                collect(Collectors.toList());

        assertEquals(4, users.size());
        assertEquals(sortedUsers, users);
    }

    @Test
    public void testFindByUsernameOrEmail() {
        String validEmail = "john@microsoft.com";
        String validUsername = "john.microsoft";

        Optional<User> expectedUser = userRepository.findByEmail(validEmail);
        assertEquals(expectedUser,
                userRepository.findByUsernameOrEmail( validUsername, validEmail));
        assertEquals(expectedUser,
                userRepository.findByUsernameOrEmail( validUsername, "invalid"));
        assertEquals(expectedUser,
                userRepository.findByUsernameOrEmail( "invalid", validEmail));
        assertEquals(expectedUser,
                userRepository.findByUsernameOrEmail( null, validEmail));
        assertEquals(expectedUser,
                userRepository.findByUsernameOrEmail( validUsername, null));
        assertEquals(Optional.empty(),
                userRepository.findByUsernameOrEmail( "invalid", null));
    }
}
