package com.sda.spring.web.repositories;

import com.sda.spring.web.models.Role;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.*;
import org.springframework.data.domain.PageRequest;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DataJpaTest
public class RoleRepositoryTest {

    @Autowired
    private RoleRepositories roleRepositories;

    @Test
    public  void testDeleteByName() {
        Role role = new Role();
        role.setName("Test Role");
        roleRepositories.save(role);

        Long initialSize = roleRepositories.count();
        roleRepositories.deleteByName(role.getName());

        Assert.assertEquals(roleRepositories.count(), initialSize-1);
    }

    @Test
    public void testUpdateName() {
        String testName = "TestRole";
        Role role = roleRepositories.findById(1L).orElseThrow(() -> new RuntimeException("role not found"));
        roleRepositories.updateByName(role.getId(), testName);
        role = roleRepositories.findById(1L).orElseThrow(() -> new RuntimeException("role not found"));
        assertEquals(testName, role.getName());
    }

    @Test
    public void testFindAllPaginateAndSorted() {
        Pageable firstPageWithTwoElementsSortedByName = PageRequest.of(0, 2, Sort.by("name"));
        Page<Role> page = roleRepositories.findAll(firstPageWithTwoElementsSortedByName);

        List<Role> roleList = roleRepositories.findAll().
                stream().
                sorted(Comparator.comparing(Role::getName)).
                limit(2).
                collect(Collectors.toList());
        assertEquals(roleList, page.getContent());
    }
}
