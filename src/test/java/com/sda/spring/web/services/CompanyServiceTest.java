package com.sda.spring.web.services;

import com.sda.spring.web.MockFactory;
import com.sda.spring.web.dto.CompanyDto;
import com.sda.spring.web.exceptions.ResourceNotFoundException;
import com.sda.spring.web.repositories.CompanyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {
    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private CompanyService companyService;

    @Test
    public void testFindById() {
        //arrange
        when(companyRepository.findById(1L))
                .thenReturn(Optional.of(MockFactory.createCompany(1L, "TestCompany", "TestDomain")));
        //act
        CompanyDto result = companyService.getCompanyById(1L);
        //assert
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
        assertThat(result.getName()).isEqualTo("TestCompany");
        assertThat(result.getDomain()).isEqualTo("TestDomain");
    }

    @Test
    public void testFindByInvalidId() {
        when(companyRepository.findById(anyLong()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> companyService.getCompanyById(9L));
    }

    @Test
    public void findAllCompaniesByName() {
        when(companyRepository.findAll())
                .thenReturn(Arrays.asList(
                        MockFactory.createCompany(1L, "TestCompany", "TestDomain"),
                        MockFactory.createCompany(2L, "TestCompany2", "TestDomain2"),
                        MockFactory.createCompany(3L, "TestCompany3", "TestDomain3")
                        ));
        List<CompanyDto> results = companyService.getCompanies(null);

        assertThat(results).isNotNull();
        assertThat(results).hasSize(3);
        assertThat(results.get(1).getName()).isEqualTo("TestCompany2");
    }

    @Test
    public void findAllCompaniesByNameWithValidDomain() {
        when(companyRepository.findByDomainContainingIgnoreCase(anyString()))
                .thenReturn(Arrays.asList(
                        MockFactory.createCompany(1L, "TestCompany", "TestDomain"),
                        MockFactory.createCompany(2L, "TestCompany2", "TestDomain2")
                ));
        List<CompanyDto> results = companyService.getCompanies("Dom");

        assertThat(results).isNotNull();
        assertThat(results).hasSize(2);
        assertThat(results.get(1).getName()).isEqualTo("TestCompany2");
    }

}