package com.sda.spring.web;

import com.sda.spring.web.models.Company;
import com.sda.spring.web.models.Role;
import com.sda.spring.web.models.User;

public class MockFactory {
    private MockFactory() {
    }
    public static Role createRole(long id, String name) {
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        return role;
    }
    public static User createUser(long id, String name,
                                  String username,
                                  String password,
                                  String email) {
        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        return user;
    }
    public static Company createCompany(long id,
                                        String name,
                                        String domain) {
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setDomain(domain);
        return company;
    }
}
